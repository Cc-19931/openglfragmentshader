//
//  LMCAMHGLRender.m
//  MiHome
//
//  Created by CoolKernel on 08/05/2017.
//  Copyright © 2017 小米移动软件. All rights reserved.
//

#import "LMCAMHGLRender.h"
#import <AVFoundation/AVUtilities.h>
#import <mach/mach_time.h>
#include <AVFoundation/AVFoundation.h>
#import <UIKit/UIScreen.h>
#include <OpenGLES/EAGL.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

#define SHADER_STRING(x) @#x

static NSString *const shader_vsh = SHADER_STRING(
attribute vec4 position;
attribute vec2 texCoord;
uniform float zoomX;
uniform float zoomY;
uniform float transitionX;
uniform float transitionY;
uniform float preferredRotation;
varying vec2 textureCoordinate;
varying vec4 vPosition;
void main()
{
    mat4 rotationScaleMatrix = mat4(cos(preferredRotation) * zoomX, -sin(preferredRotation), 0.0, 0.0,
                               sin(preferredRotation),  cos(preferredRotation) * zoomY, 0.0, 0.0,
                               0.0,					    0.0,                    1.0, 0.0,
                               0.0,					    0.0,                    0.0, 1.0);
    gl_Position = position * rotationScaleMatrix + vec4(transitionX, transitionY, 0.0, 0.0);
    textureCoordinate = texCoord;
    vPosition = position;
});

static NSString *const shader_fsh = SHADER_STRING(
                                                  precision mediump float;
                                                  varying vec2 textureCoordinate;
                                                  varying vec4 vPosition;
                                                  uniform sampler2D SamplerY;
                                                  uniform sampler2D SamplerUV;
                                                  uniform mat3 colorConversionMatrix;
                                                  uniform float correctionRadius;
                                                  uniform float zoom;
                                                  uniform float osdx;
                                                  uniform float osdy;
                                                  uniform int lensCorrect;
                                                  uniform int timestampCorrect;
                                                  void main() {
                                                      mediump vec3 yuv;
                                                      lowp vec3 rgb;
                                                      
                                                      vec4 vPos = vPosition;
                                                      vec2 vMapping = vPos.xy;
                                                      vMapping.x = vMapping.x + ((pow(vPos.y, 2.0)/zoom)*vPos.x/zoom)*-osdx;
                                                      vMapping.y = vMapping.y + ((pow(vPos.x, 2.0)/zoom)*vPos.y/zoom)*-osdy;
                                                      vMapping = vMapping/zoom * vec2(1.0, -1.0)/ 2.0 + vec2(0.5, 0.5);
                                                      
                                                      yuv.x = (texture2D(SamplerY, vMapping).r - (16.0 / 255.0));
                                                      yuv.yz = (texture2D(SamplerUV, vMapping).rg - vec2(0.5, 0.5));
                                                      rgb = colorConversionMatrix * yuv;
                                                      
                                                      gl_FragColor = vec4(rgb, 1);
                                                  });
/*
static NSString *const shader_fsh = SHADER_STRING(
precision mediump float;
varying vec2 textureCoordinate;
uniform sampler2D SamplerY;
uniform sampler2D SamplerUV;
uniform mat3 colorConversionMatrix;
uniform float correctionRadius;
uniform float zoom;
uniform float osdx;
uniform float osdy;
uniform int lensCorrect;
uniform int timestampCorrect;
void main() {
    mediump vec3 yuv;
    lowp vec3 rgb;
    vec2 newTextureCoor;
    if (lensCorrect == 0) {
        newTextureCoor = textureCoordinate;
    } else {
        if (textureCoordinate.x < osdx && textureCoordinate.y < osdy) {
              newTextureCoor = textureCoordinate;
        } else {
            vec2 newCoor = textureCoordinate - vec2(0.5, 0.5);
            float dis = length(newCoor);
            float r = dis / correctionRadius;
            float theta = zoom*1.0;
            if(r > 0.0) {
                theta = zoom*atan(r) / r;
            }
            newTextureCoor = vec2(0.5, 0.5) + newCoor * theta;
        }
    }
                                                      
    yuv.x = (texture2D(SamplerY, newTextureCoor).r - (16.0 / 255.0));
    yuv.yz = (texture2D(SamplerUV, newTextureCoor).rg - vec2(0.5, 0.5));
    rgb = colorConversionMatrix * yuv;
    
    gl_FragColor = vec4(rgb, 1);
});
*/
// Uniform index.
enum
{
    //标准
    UNIFORM_Y,
    UNIFORM_UV,
    UNIFORM_ROTATION_ANGLE,         //旋转角度
    UNIFORM_COLOR_CONVERSION_MATRIX,
    UNIFORM_ZOOM_X,                 //x方向缩放比例
    UNIFORM_ZOOM_Y,                 //y方向缩放比例
    UNIFORM_TRANSITION_X,           //x方向顶点偏移
    UNIFORM_TRANSITION_Y,           //y方向顶点偏移
    //畸变
    UNINFOM_CORRECT_USED,
    UNINFOM_CORRECT_RADIUS,         //畸变矫正角度
    UNINFOM_CORRECT_OSDX,
    UNINFOM_CORRECT_OSDY,
    UNINFOM_CORRECT_ZOOM,
    
    NUM_UNIFORMS
};
GLint lmca_uniforms[NUM_UNIFORMS];

// Attribute index.
enum
{
    ATTRIB_VERTEX,
    ATTRIB_TEXCOORD,
    NUM_ATTRIBUTES
};

// Color Conversion Constants (YUV to RGB) including adjustment from 16-235/16-240 (video range)

// BT.601, which is the standard for SDTV.
static const GLfloat kColorConversion601[] = {
    1.164,  1.164, 1.164,
    0.0, -0.392, 2.017,
    1.596, -0.813,   0.0,
};

// BT.709, which is the standard for HDTV.
static const GLfloat kColorConversion709[] = {
    1.164,  1.164, 1.164,
    0.0, -0.213, 2.112,
    1.793, -0.533,   0.0,
};

@interface LMCAMHGLRender ()
{
    EAGLContext *_context;
    CVOpenGLESTextureRef _lumaTexture;
    CVOpenGLESTextureRef _chromaTexture;
    
    GLuint _frameBufferHandle;
    GLuint _colorBufferHandle;
    CAEAGLLayer *_glLayer;
    const GLfloat *_preferredConversion;
    BOOL _isPause;
}
@property GLuint program;
@property CVPixelBufferRef pixelBuffer;

@end

@implementation LMCAMHGLRender
{
    id <MHGLProgramHandlerProtocol> _responder;
    float _zoomX;
    float _zoomY;
    float _transitionX;
    float _transitionY;
    BOOL _isNeedCustomShader;
}

- (LMCAMHGLRender *)initWithGLLayer:(CAEAGLLayer *)glLayer
{
    return [self initWithGLLayer:glLayer otherProgramResponder:nil];
}

- (LMCAMHGLRender *)initWithGLLayer:(CAEAGLLayer *)glLayer otherProgramResponder:(id <MHGLProgramHandlerProtocol>)responder
{
    if (self = [super init]) {
        _responder = responder;
        _glLayer = glLayer;
        _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
        _context.multiThreaded = YES;
        // Set the default conversion to BT.709, which is the standard for HDTV.
        _preferredConversion = kColorConversion709;
        _zoomY = _zoomX = 1.0;
        _transitionX = _transitionY = 0.0;
        _isPause = NO;
        [self setupGL];
    }
    
    return self;
}

- (void)renderBuffer:(CVPixelBufferRef)buffer
{
    if (_isPause) {
        LMLogInfo(@"======LMCAMHGLRender renderBuffer: return");
        return;
    }
    [self renderBuffer:buffer finished:nil];
}

- (void)renderBuffer:(CVPixelBufferRef)buffer
            finished:(void (^)(void))finished
{
    if (buffer == NULL || _isPause) return;
    
    @synchronized (self) {
        if (_pixelBuffer) {
            CVPixelBufferRelease(_pixelBuffer);
            _pixelBuffer = NULL;
        }
        
        _pixelBuffer = CVPixelBufferRetain(buffer);
        
        int d_w = (int)CVPixelBufferGetWidth(_pixelBuffer);
        int d_h = (int)CVPixelBufferGetHeight(_pixelBuffer);
        
        [self displayPixelBuffer:buffer width:d_w height:d_h finished:finished];
    }
}

- (void)displayPixelBuffer:(CVPixelBufferRef)pixelBuffer
                     width:(uint32_t)frameWidth
                    height:(uint32_t)frameHeight
                  finished:(void (^)(void))finished
{
    if (!_context || ![EAGLContext setCurrentContext:_context] || _isPause) return;
    if (pixelBuffer == NULL) return;
    
    CVReturn err;
    // 获取通道数目
    size_t planeCount = CVPixelBufferGetPlaneCount(pixelBuffer);
    
    // Use the color attachment of the pixel buffer to determine the appropriate color conversion matrix.
    // 获取pixelBuffer的颜色空间格式，决定使用的颜色转换矩阵，用于下一步的YUV到RGB颜色空间的转换；
    CFTypeRef colorAttachments = CVBufferGetAttachment(pixelBuffer, kCVImageBufferYCbCrMatrixKey, NULL);
    
    if (colorAttachments && CFStringCompare(colorAttachments, kCVImageBufferYCbCrMatrix_ITU_R_601_4, 0) == kCFCompareEqualTo) {
        _preferredConversion = kColorConversion601;
    } else {
        _preferredConversion = kColorConversion709;
    }

    CVOpenGLESTextureCacheRef _videoTextureCache;
    // Create CVOpenGLESTextureCacheRef for optimal CVPixelBufferRef to GLES texture conversion.
    // 分配一个CVOpenGLESTextureCacheRef对象
    err = CVOpenGLESTextureCacheCreate(kCFAllocatorDefault, NULL, _context, NULL, &_videoTextureCache);
    if (err != noErr) {
        LMLogInfo(@"Error at CVOpenGLESTextureCacheCreate %d", err);
        return;
    }
    // 调用此函数后着色器程序才能正常使用
    glUseProgram(self.program);
    // 启用纹理
    glActiveTexture(GL_TEXTURE0);
    // 创建纹理
    err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                       _videoTextureCache,
                                                       pixelBuffer,
                                                       NULL,
                                                       GL_TEXTURE_2D,
                                                       GL_RED_EXT,
                                                       frameWidth,
                                                       frameHeight,
                                                       GL_RED_EXT,
                                                       GL_UNSIGNED_BYTE,
                                                       0,
                                                       &_lumaTexture);
    if (err) {
        LMLogInfo(@"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
    }
    // 绑定纹理，设置好纹理格式；
    glBindTexture(CVOpenGLESTextureGetTarget(_lumaTexture), CVOpenGLESTextureGetName(_lumaTexture));
    // 给指定的纹理类型对象设定参数
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    if(planeCount == 2) {
        // UV-plane.
        glActiveTexture(GL_TEXTURE1);
        err = CVOpenGLESTextureCacheCreateTextureFromImage(kCFAllocatorDefault,
                                                           _videoTextureCache,
                                                           pixelBuffer,
                                                           NULL,
                                                           GL_TEXTURE_2D,
                                                           GL_RG_EXT,
                                                           frameWidth / 2,
                                                           frameHeight / 2,
                                                           GL_RG_EXT,
                                                           GL_UNSIGNED_BYTE,
                                                           1,
                                                           &_chromaTexture);
        if (err) {
            LMLogInfo(@"Error at CVOpenGLESTextureCacheCreateTextureFromImage %d", err);
        }
        
        glBindTexture(CVOpenGLESTextureGetTarget(_chromaTexture), CVOpenGLESTextureGetName(_chromaTexture));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
    
    if (planeCount > 2) {
        if (_responder && [_responder respondsToSelector:@selector(loadTextureWithTextureCache:buffer:)]) {
            [_responder loadTextureWithTextureCache:_videoTextureCache buffer:pixelBuffer];
        }
    }
    // 绑定buffer
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBufferHandle);
    
    int d_w = (int)CVPixelBufferGetWidth(_pixelBuffer);
    int d_h = (int)CVPixelBufferGetHeight(_pixelBuffer);
    float f_w = _glLayer.bounds.size.width;
    float f_h = _glLayer.bounds.size.height;
    
    float s1 = (float)d_w / f_w;
    float s2 = (float)d_h / f_h;
    float scale = s1 < s2 ? s2 : s1;
    
    float beginX = (_glLayer.bounds.size.width * _glLayer.contentsScale - (_glLayer.bounds.size.width * scale) * (_glLayer.bounds.size.width * _glLayer.contentsScale/d_w)) / 2.0;
    float beginY = (_glLayer.bounds.size.height * _glLayer.contentsScale - (_glLayer.bounds.size.height * scale) * (_glLayer.bounds.size.height * _glLayer.contentsScale/d_h)) / 2.0;
    
    float scaleX = beginX/(_glLayer.bounds.size.width * _glLayer.contentsScale);
    float scaleY = beginY/(_glLayer.bounds.size.height * _glLayer.contentsScale);
    
    // Set the view port to the entire view.
    // 在窗口中定义一个像素矩形，最终将图像映射到这个矩形中
    glViewport(-beginX, -beginY, (_glLayer.bounds.size.width * _glLayer.contentsScale) + beginX * 2, (_glLayer.bounds.size.height * _glLayer.contentsScale) + beginY * 2);
    
    // 设置清除颜色
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    // Use shader program.使用shader程序
    glUseProgram(self.program);
    glUniformMatrix3fv(lmca_uniforms[UNIFORM_COLOR_CONVERSION_MATRIX], 1, GL_FALSE, _preferredConversion);

    GLfloat quadVertexData[] = {
        -1.0, -1.0,
        1.0, -1.0,
        -1.0, 1.0,
        1.0, 1.0
    };
    // 设置顶点属性,顶点着色器设置数据
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, 0, 0, quadVertexData);
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    
    GLfloat quadTextureData[] = {
        0.0, 1.0,
        1.0, 1.0,
        0.0, 0.0,
        1.0, 0.0
    };
    glVertexAttribPointer(ATTRIB_TEXCOORD, 2, GL_FLOAT, 0, 0, quadTextureData);
    glEnableVertexAttribArray(ATTRIB_TEXCOORD);
    
    if (_isNeedCustomShader) {
        if ([_responder respondsToSelector:@selector(bindFrameBufferWithPreferredConversion:)]) {
            [_responder bindFrameBufferWithPreferredConversion:_preferredConversion];
            glBindRenderbuffer(GL_RENDERBUFFER, _colorBufferHandle);
            glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorBufferHandle);
            GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            if (status != GL_FRAMEBUFFER_COMPLETE) {
                LMLogInfo(@"status = %d", status);
            }
        }
    } else {
        if (_responder) {
            if ([_responder respondsToSelector:@selector(deliverParametesLenCorrect:normalCompletion:)]) {
                [_responder deliverParametesLenCorrect:^(BOOL isUsedLenCorrect, float correctRadius, float osdx, float osdy, float zoom) {
                    glUniform1i(lmca_uniforms[UNINFOM_CORRECT_USED], isUsedLenCorrect ? 1 : 0);
                    glUniform1f(lmca_uniforms[UNINFOM_CORRECT_RADIUS], correctRadius);
                    glUniform1f(lmca_uniforms[UNINFOM_CORRECT_OSDX], osdx);
                    glUniform1f(lmca_uniforms[UNINFOM_CORRECT_OSDY], osdy);
                    glUniform1f(lmca_uniforms[UNINFOM_CORRECT_ZOOM], zoom == 0.0 ? 1.0 : zoom);
                } normalCompletion:^(float zoomX, float zoomY, float transitionX, float transitionY, float roattion) {
                    glUniform1f(lmca_uniforms[UNIFORM_ZOOM_X], zoomX == 0.0 ? 1.0 : zoomX);
                    glUniform1f(lmca_uniforms[UNIFORM_ZOOM_Y], zoomY == 0.0 ? 1.0 : zoomY);
                    glUniform1f(lmca_uniforms[UNIFORM_TRANSITION_X], transitionX);
                    glUniform1f(lmca_uniforms[UNIFORM_TRANSITION_Y], transitionY);
                    glUniform1f(lmca_uniforms[UNIFORM_ROTATION_ANGLE], roattion);
                }];
            }
        } else {
            glUniform1i(lmca_uniforms[UNINFOM_CORRECT_USED], 0);
            glUniform1f(lmca_uniforms[UNIFORM_ZOOM_X], 1.0);
            glUniform1f(lmca_uniforms[UNIFORM_ZOOM_Y], 1.0);
            glUniform1f(lmca_uniforms[UNIFORM_TRANSITION_X], 0.0);
            glUniform1f(lmca_uniforms[UNIFORM_TRANSITION_Y], 0.0);
            glUniform1f(lmca_uniforms[UNIFORM_ROTATION_ANGLE], 0.0);
        }
    }
    // 设置渲染方式
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    // 在渲染准备工作都准备就绪后,使用presentRenderbuffer进行图片渲染,将渲染区的图片绘制到layer上
    [_context presentRenderbuffer:GL_RENDERBUFFER];
    
    [self cleanUpTextures];
    // Periodic texture cache flush every frame
    CVOpenGLESTextureCacheFlush(_videoTextureCache, 0);
    
    if(_videoTextureCache) { 
        CFRelease(_videoTextureCache);
    }
    
    if (finished) {
        finished();
    }
}

# pragma mark - OpenGL setup

- (void)setupGL {
    if (!_context || ![EAGLContext setCurrentContext:_context]) return;
    
    [self setupBuffers];
    [self loadShaders];
    
    // 调用此函数后着色器程序才能正常使用
    glUseProgram(self.program);
    
    // 0 and 1 are the texture IDs of _lumaTexture and _chromaTexture respectively.
    // 给片段着色器中的uniform sampler2D 修饰的纹理变量赋值，
    // 只有该函数调用后,片元着色器中的texture2D()函数才能正确工作
    glUniform1i(lmca_uniforms[UNIFORM_Y], 0);
    glUniform1i(lmca_uniforms[UNIFORM_UV], 1);
    glUniformMatrix3fv(lmca_uniforms[UNIFORM_COLOR_CONVERSION_MATRIX], 1, GL_FALSE, _preferredConversion);
}

#pragma mark - Utilities

/// 初始化buffer
- (void)setupBuffers {
    glDisable(GL_DEPTH_TEST);
    
    // 设置顶点属性
    glEnableVertexAttribArray(ATTRIB_VERTEX);
    glVertexAttribPointer(ATTRIB_VERTEX, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), 0);
    
    glEnableVertexAttribArray(ATTRIB_TEXCOORD);
    glVertexAttribPointer(ATTRIB_TEXCOORD, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), 0);
    
    [self createBuffers];
}

/// 创建buffers
- (void)createBuffers {
    glGenFramebuffers(1, &_frameBufferHandle);
    glBindFramebuffer(GL_FRAMEBUFFER, _frameBufferHandle);
    
    glGenRenderbuffers(1, &_colorBufferHandle);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorBufferHandle);
    
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_glLayer];
    // 将renderbuffer对象附加到framebuffer对象
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorBufferHandle);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        LMLogInfo(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatus(GL_FRAMEBUFFER));
    }
    
    if (_responder) {
        if ([_responder respondsToSelector:@selector(genFrameBuffer)]) {
            [_responder genFrameBuffer];
        }
    }
}

- (void)releaseBuffers
{
    if(_frameBufferHandle) {
        glDeleteFramebuffers(1, &_frameBufferHandle);
        _frameBufferHandle = 0;
    }
    
    if(_colorBufferHandle) {
        glDeleteRenderbuffers(1, &_colorBufferHandle);
        _colorBufferHandle = 0;
    }
    
    if (_responder) {
        if ([_responder respondsToSelector:@selector(deleteFrameBuffer)]) {
            [_responder deleteFrameBuffer];
        }
    }
}

- (void)resetRenderBuffer
{
    if (!_context || ![EAGLContext setCurrentContext:_context]) {
        return;
    }
    
    [self releaseBuffers];
    [self createBuffers];
}

- (void)cleanUpTextures
{
    if (_lumaTexture) {
        CFRelease(_lumaTexture);
        _lumaTexture = NULL;
    }
    
    if (_chromaTexture) {
        CFRelease(_chromaTexture);
        _chromaTexture = NULL;
    }
    
    if (_responder && [_responder respondsToSelector:@selector(deleteTexture)]) {
        [_responder deleteTexture];
    }
}

- (BOOL)loadShaders {
    // Create the shader program.创建显卡执行程序
    self.program = glCreateProgram();
    if (_responder && [_responder respondsToSelector:@selector(supportCustomShader)]) {
        _isNeedCustomShader = [_responder supportCustomShader];
    }
    
    if (_isNeedCustomShader) {
        if (_responder) {
            if ([_responder respondsToSelector:@selector(postTargetMainProgram:)]) {
                [_responder postTargetMainProgram:self.program];
            }
            if ([_responder respondsToSelector:@selector(loadShader)]) {
                [_responder loadShader];
            }
        }
        return YES;
    }
    
    //support normal shader
    GLuint vertShader = 0, fragShader = 0;
    
    if(![self compileShaderString:&vertShader type:GL_VERTEX_SHADER shaderString:[shader_vsh UTF8String]]) {
        LMLogInfo(@"Failed to compile vertex shader");
        return NO;
    }
    
    if(![self compileShaderString:&fragShader type:GL_FRAGMENT_SHADER shaderString:[shader_fsh UTF8String]]) {
        LMLogInfo(@"Failed to compile fragment shader");
        return NO;
    }
    
    // Attach vertex shader to program.将着色器程序附着到显卡执行程序中
    glAttachShader(self.program, vertShader);
    
    // Attach fragment shader to program.
    glAttachShader(self.program, fragShader);
    
    // Bind attribute locations. This needs to be done prior to linking.
    glBindAttribLocation(self.program, ATTRIB_VERTEX, "position");
    glBindAttribLocation(self.program, ATTRIB_TEXCOORD, "texCoord");
    
    // Link the program.
    if (![self linkProgram:self.program]) {
        LMLogInfo(@"Failed to link program: %d", self.program);
        if (vertShader) {
            glDeleteShader(vertShader);
            vertShader = 0;
        }
        if (fragShader) {
            glDeleteShader(fragShader);
            fragShader = 0;
        }
        if (self.program) {
            glDeleteProgram(self.program);
            self.program = 0;
        }
        
        return NO;
    }

    // Get uniform locations.
    lmca_uniforms[UNIFORM_Y] = glGetUniformLocation(self.program, "SamplerY");
    lmca_uniforms[UNIFORM_UV] = glGetUniformLocation(self.program, "SamplerUV");
    lmca_uniforms[UNIFORM_ROTATION_ANGLE] = glGetUniformLocation(self.program, "preferredRotation");
    lmca_uniforms[UNIFORM_COLOR_CONVERSION_MATRIX] = glGetUniformLocation(self.program, "colorConversionMatrix");
    lmca_uniforms[UNIFORM_ZOOM_X] = glGetUniformLocation(self.program, "zoomX");
    lmca_uniforms[UNIFORM_ZOOM_Y] = glGetUniformLocation(self.program, "zoomY");
    lmca_uniforms[UNIFORM_TRANSITION_X] = glGetUniformLocation(self.program, "transitionX");
    lmca_uniforms[UNIFORM_TRANSITION_Y] = glGetUniformLocation(self.program, "transitionY");
    
    lmca_uniforms[UNINFOM_CORRECT_USED] = glGetUniformLocation(self.program, "lensCorrect");
    lmca_uniforms[UNINFOM_CORRECT_RADIUS] = glGetUniformLocation(self.program, "correctionRadius");
    lmca_uniforms[UNINFOM_CORRECT_OSDX] = glGetUniformLocation(self.program, "osdx");
    lmca_uniforms[UNINFOM_CORRECT_OSDY] = glGetUniformLocation(self.program, "osdy");
    lmca_uniforms[UNINFOM_CORRECT_ZOOM] = glGetUniformLocation(self.program, "zoom");
    // Release vertex and fragment shaders.
    if (vertShader) {
        glDetachShader(self.program, vertShader);
        glDeleteShader(vertShader);
    }
    if (fragShader) {
        glDetachShader(self.program, fragShader);
        glDeleteShader(fragShader);
    }
    
    return YES;
}

- (BOOL)compileShaderString:(GLuint *)shader type:(GLenum)type shaderString:(const GLchar *)shaderString
{
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &shaderString, NULL);
    glCompileShader(*shader);
#if defined(DEBUG)
    GLint logLength;
    glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetShaderInfoLog(*shader, logLength, &logLength, log);
        LMLogInfo(@"Shader compile log:\n%s", log);
        free(log);
    }
#endif
    GLint status = 0;
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &status);
    if (status == 0) {
        glDeleteShader(*shader);
        return NO;
    }
    
    return YES;
}

- (BOOL)compileShader:(GLuint *)shader type:(GLenum)type URL:(NSURL *)URL
{
    NSError *error;
    NSString *sourceString = [[NSString alloc] initWithContentsOfURL:URL encoding:NSUTF8StringEncoding error:&error];
    if (sourceString == nil) {
        LMLogInfo(@"Failed to load vertex shader: %@", [error localizedDescription]);
        return NO;
    }
    
    const GLchar *source = (GLchar *)[sourceString UTF8String];
    
    return [self compileShaderString:shader type:type shaderString:source];
}

- (BOOL)linkProgram:(GLuint)prog
{
    GLint status;
    glLinkProgram(prog);
    
#if defined(DEBUG)
    GLint logLength;
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        LMLogInfo(@"Program link log:\n%s", log);
        free(log);
    }
#endif
    glGetProgramiv(prog, GL_LINK_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateProgram:(GLuint)prog
{
    GLint logLength, status;
    
    glValidateProgram(prog);
    glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
    if (logLength > 0) {
        GLchar *log = (GLchar *)malloc(logLength);
        glGetProgramInfoLog(prog, logLength, &logLength, log);
        LMLogInfo(@"Program validate log:\n%s", log);
        free(log);
    }
    
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &status);
    if (status == 0) {
        return NO;
    }
    
    return YES;
}

- (void)willEnterForeground
{
    LMLogInfo(@"======LMCAMHGLRender willEnterForeground");
    _isPause = NO;
}

- (void)enterForeground
{
    LMLogInfo(@"======LMCAMHGLRender enterForeground");
    _isPause = NO;
    glUseProgram(self.program);
    glUseProgram(0);
    glFinish();
}

- (void)enterBackground
{
    LMLogInfo(@"======LMCAMHGLRender enterBackground");
    _isPause = YES;
    glFinish();
}

- (void)didEnterBackground
{
    LMLogInfo(@"======LMCAMHGLRender didEnterBackground");
    _isPause = YES;
    glFinish();
}

- (void)flush
{
    MHRGBA rgb = {
        (51/255.0),
        (51/255.0),
        (51/255.0),
        1.0f
    };
    
    [self flush:rgb];
}

- (void)flush:(MHRGBA)rgb
{
    @synchronized (self) {
        [EAGLContext setCurrentContext:_context];
        glClearColor(rgb.r, rgb.g, rgb.b, rgb.a);
        if (_colorBufferHandle) {
            glClear(GL_COLOR_BUFFER_BIT);
        }
        [_context presentRenderbuffer:GL_RENDERBUFFER];
        glFlush();
    }
}

- (UIImage *)snapshotPicture
{
    UIImage* snapshot = nil;
    @synchronized (self) {
        CVPixelBufferRef snapshotBuffer = CVPixelBufferRetain(self.pixelBuffer);
        CVPixelBufferLockBaseAddress(snapshotBuffer, 0);
        snapshot = [self pixelBufferToImage:snapshotBuffer];
        CVPixelBufferUnlockBaseAddress(snapshotBuffer, 0);
        CVPixelBufferRelease(snapshotBuffer);
    }
    return snapshot;
}

- (UIImage *)pixelBufferToImage:(CVPixelBufferRef)pixelBuffer
{
    if (pixelBuffer == NULL || !_context || ![EAGLContext setCurrentContext:_context]) {
        LMLogInfo(@"pixelbuffer:%p or context error", pixelBuffer);
        return nil;
    }
    CIImage *ciImage = [CIImage imageWithCVPixelBuffer:pixelBuffer];
    UIImage *uiImage = nil;
    if (ciImage) {
        CIContext *temporaryContext = [CIContext contextWithEAGLContext:_context];
        CGImageRef cgImage = [temporaryContext
                                 createCGImage:ciImage
                                 fromRect:CGRectMake(0, 0,
                                                     CVPixelBufferGetWidth(pixelBuffer),
                                                     CVPixelBufferGetHeight(pixelBuffer))];
        uiImage = [UIImage imageWithCGImage:cgImage];
        CGImageRelease(cgImage);
    }
    
    return uiImage;
}

- (CIImage *)snapShot
{
    CIImage *ciimage = nil;
    CVPixelBufferRef snapshotBuffer = CVPixelBufferRetain(self.pixelBuffer);
    CVPixelBufferLockBaseAddress(snapshotBuffer, 0);
    ciimage = [CIImage imageWithCVPixelBuffer:snapshotBuffer];
    CVPixelBufferUnlockBaseAddress(snapshotBuffer, 0);
    CVPixelBufferRelease(snapshotBuffer);
    
    return ciimage;
}

- (UIImage *)snapshotCorrectPicture
{
    if (self.pixelBuffer == NULL) return nil;
    glPixelStorei(GL_PACK_ALIGNMENT, 4);
    int width = (int)CVPixelBufferGetWidth(_pixelBuffer);
    int height = (int)CVPixelBufferGetHeight(_pixelBuffer);
    NSInteger dataLength = width * height * 4;
    GLubyte *data = (GLubyte*)malloc(dataLength * sizeof(GLubyte));
    glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, data);
    
    CGDataProviderRef ref = CGDataProviderCreateWithData(NULL, data, dataLength, NULL);
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGImageRef iref = CGImageCreate(width, height, 8, 32, width * 4, colorspace, kCGBitmapByteOrder32Big | kCGImageAlphaPremultipliedLast,                               ref, NULL, true, kCGRenderingIntentDefault);
    
    UIGraphicsBeginImageContextWithOptions(_glLayer.bounds.size, NO, _glLayer.contentsScale);
    CGContextRef cgcontext = UIGraphicsGetCurrentContext();
    CGContextSetBlendMode(cgcontext, kCGBlendModeCopy);
    CGContextDrawImage(cgcontext, CGRectMake(0.0, 0.0, _glLayer.bounds.size.width, _glLayer.bounds.size.height), iref);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Clean up
    free(data);
    CFRelease(ref);
    CFRelease(colorspace);
    CGImageRelease(iref);
    
    return image;
}

- (CVPixelBufferRef)snapshotPB {
    return CVPixelBufferRetain(self.pixelBuffer);
}

- (void)dealloc
{
    if (!_context || ![EAGLContext setCurrentContext:_context]) return;
    [self cleanUpTextures];
    
    if(_pixelBuffer) {
        CVPixelBufferRelease(_pixelBuffer);
    }
    
    if (self.program) {
        glDeleteProgram(self.program);
        self.program = 0;
    }
    
    if (_responder && [_responder respondsToSelector:@selector(deleteProgram)]) {
        [_responder deleteProgram];
    }
//    if(_context) {
//        _context = nil;
//    }

    //fix：Debug模式二次进入crash
    if ([EAGLContext currentContext] == _context)
    {
        [EAGLContext setCurrentContext:nil];
    }
    _context = nil;

    if (_responder) {
        _responder = nil;
    }
}

- (id <MHGLProgramHandlerProtocol>)getEffectTarget
{
    return _responder;
}

- (void)reconfigRenderbufferGlLayer{
    if (_context && _glLayer){
        [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_glLayer];
    }
}

@end
